package ku.util;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * The upward data reader and modifier for the arrays that skip over null elements
 * Just like Scanner, but this one reads elements in array, not string texts.
 * @author  Parinvut Rochanavedya 
 * @version 2015.03.13
 */
public class ArrayIterator<T> implements Iterator<T>{

	/**Array to be read and iterated*/
	private T[ ] array;
	
	/**Cursor that points to the current index of the array standing on*/
	private int cursor = -1;
	private int nextCursor = 0;
	
	/**
	 * Constructor
	 * @param array : the array to be read and modify
	 */
	public ArrayIterator(T[] array){
		this.array = array;
	}
	
	/**
	 * Return the next non-null element in the array.
	 * If there are no more elements, throws NoSuchElementException.
	 */
	public T next() {
		if(hasNext()){
			cursor = nextCursor;
			return array[cursor];
		}
		
		throw new NoSuchElementException(); //out of bounds
	}
	
	/**
	 * Returns true if next() can return another non-null array element,
	 * false if no more elements.
	 */
	public boolean hasNext() {
		int tempCursor = cursor+1;
		while(tempCursor < array.length){
			if(array[tempCursor] != null){
				nextCursor = tempCursor;
				return true;
			}
			tempCursor++;
		}
		nextCursor = array.length;
		return false;
	}

	/**
	 * Remove most recent element returned by next() from the array by
	 * setting it to null. 
	 */
	public void remove(){
		array[cursor] = null;
	}

}
