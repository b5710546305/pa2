package ku.util;

import java.util.EmptyStackException;

/**
 * A stack is a data type that works like BBQ stick
 * you simply put the elements into the stack
 * the last one that is in the stack will always gone first
 * @author  Parinvut Rochanavedya 
 * @version 2015.03.13
 * @param <T> Specific type setter for each stack
 */
public class Stack<T> {

	/**items on the stack*/
	private T[] items;
	
	/**
	 * Constructor
	 * @param capacity : the max size of the stack
	 */
	public Stack(int capacity){
		if(capacity <= 0){throw new IllegalArgumentException();}
		items = (T[])new Object[capacity];
	}
	
	/**
	 * @return the maximum number of elements that this Stack can hold. 
	 * Return -1 if unknown or infinite.
	 */
	public int capacity(){
		if(items == null){return -1;}
		return items.length;
	}
	
	/**
	 * @return true if stack is empty.
	 */
	public boolean isEmpty(){
		int nullCheck = 0;
		for(int i = 0; i < capacity(); i++){
			if(items[i] == null){
				nullCheck++;
			}
		}
		if(nullCheck == capacity()){
			return true;
		}
		return false;
	}
	
	/**
	 * @return true if stack is full
	 */
	public boolean isFull(){
		int notNullCheck = 0;
		for(int i = 0; i < capacity(); i++){
			if(items[i] != null){
				notNullCheck++;
			}
		}
		if(notNullCheck == capacity()){
			return true;
		}
		return false;
	}
	
	/**
	 * @return the item on the top of the stack, without removing it. 
	 * 		   If the stack is empty, return null.
	 */
	public T peek(){
		if(isEmpty()){return null;}

		return items[size()-1];
		
		//return null;
	}
	
	/**
	 * @return return the item on the top of the stack, and remove it from the stack.
	 *	       Throws: EmptyStackException if stack is empty
	 */
	public T pop(){
		if(isEmpty()){throw new EmptyStackException();}
		
		T itemToRemove = items[size()-1]; //place into another variable
		items[size()-1] = null; //removed
		return itemToRemove; //use the local variable to transfer
		
		//throw new EmptyStackException();
	}
	
	/**
	 * Push a new item onto the top of the stack. 
	 * @param obj : the object to push into stack data type
	 *  The parameter (obj) must not be null.
	 *	Throws: InvalidArgumentException if parameter is null.
	 */
	public void push(T obj){
		if(obj == null){throw new IllegalArgumentException();}
		items[size()] = obj;
	}
	
	/**
	 * @return return the number of items in the stack. 
	 * Returns 0 if the stack is empty.
	 */
	public int size(){
		if(isEmpty()) {return 0;}
		int size = 0;
		for(int i = 0; i < capacity(); i++){
			if(items[i] != null){
				size++;
			}
		}
		return size;
	}
}
